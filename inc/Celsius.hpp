#ifndef CELSIUS_HPP
#define CELSIUS_HPP
#include <iostream>
#include "medidortemp.hpp"

using namespace std;

class Celsius:public Medidortemp{
private:
	
public:
	Celsius();
	Celsius(float temperatura);
	float converteFahrenheit();
	float converteKelvin();	
	

};
#endif
