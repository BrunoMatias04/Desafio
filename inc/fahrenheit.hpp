#ifndef FAHRENHEIT_HPP
#define FAHRENHEIT_HPP
#include <iostream>
#include "medidortemp.hpp"

using namespace std;

class fahrenheit : public Medidortemp{
private:
       	
public:
        fahrenheit();
	fahrenheit(float temperatura);
        float converteCelsius();
	float converteKelvin();
	

};
#endif

