#ifndef MEDIDORTEMP_HPP
#define MEDIDORTEMP_HPP
#include <iostream>


using namespace std;

class Medidortemp{
private:
       float temperatura; 
public:
	Medidortemp();
	Medidortemp(float temperatura);
       float getTemperatura();
       void setTemperatura(float temperatura);
       

};
#endif

