#ifndef KELVIN_HPP
#define KELVIN_HPP
#include <iostream>
#include "medidortemp.hpp"

using namespace std;

class kelvin : public  Medidortemp{
private:
        
public:
        kelvin();
	kelvin(float temperatura);
        float converteCelsius();
	float converteFahrenheit();


};
#endif

