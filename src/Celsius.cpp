#include "Celsius.hpp"

Celsius::Celsius(){

	setTemperatura(0);
}

Celsius::Celsius(float temperatura){
	setTemperatura(temperatura);
}

float Celsius::converteFahrenheit(){
	float num;
	num = ((getTemperatura()*9)/5)+32;
	return num;
}
float Celsius::converteKelvin(){
	float num;
	num = getTemperatura() + 273;
	return num;
}
