#include "kelvin.hpp"

kelvin::kelvin(){
	setTemperatura(0);
}
kelvin::kelvin(float temperatura){
	setTemperatura(temperatura);
}
float kelvin::converteCelsius(){
	float num;
	num = getTemperatura() - 273;
	return num;
}
float kelvin::converteFahrenheit(){
	float num;
	num = (((getTemperatura() - 273)/5)*9)+32;
	return num;

}
