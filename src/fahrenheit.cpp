#include "fahrenheit.hpp"

fahrenheit::fahrenheit(){
	setTemperatura(0);
}
fahrenheit::fahrenheit(float temperatura){
	setTemperatura(temperatura);
}
float fahrenheit::converteCelsius(){
	float num;
	num = ((getTemperatura() - 32)/9)*5;
	return num;
}
float fahrenheit::converteKelvin(){
	float num;
	num = ((getTemperatura() - 32)/1.8)+273;
	return num;
}
